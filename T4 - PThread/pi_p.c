#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>
#include <pthread.h>

//Global seed
int globalSeed;
//Variaveis das threads
int thread_count;
long thread;
pthread_t *thread_handles;


//Mutex
pthread_mutex_t mutex;

//Variaveis do programa
int size;
unsigned int n, i;
long long unsigned int in;
double d, pi, x, y;
long unsigned int duracao;
struct timeval start, end;
long long unsigned int *data; //Vetor para auxiliar os calculos
long long unsigned int in = 0;

//Funcao principal onde é realizado o calculo
void * monte_carlo_pi(void *rank) {
	long my_rank = (long) rank;
	long long unsigned int i_monte;
	double x, y, d;

	
	int sizeSlice = (int)(n / thread_count);
	/*if (my_rank  == 0){
		sizeSlice += n%thread_count;
	}
	*/
	//for (i_monte = 0; i_monte < n ; i_monte++) {
	for (i_monte = 0; i_monte < sizeSlice  ; i_monte++) {
		//x = ((rand() % 1000000)/500000.0)-1;
		//y = ((rand() % 1000000)/500000.0)-1;
		x = ((rand_r(&globalSeed) % 1000000)/500000.0)-1;
		y = ((rand_r(&globalSeed) % 1000000)/500000.0)-1;
		d = ((x*x) + (y*y));
		if (d <= 1){
			data[my_rank]++;		
		}
	}
}

int main(void) {


	scanf("%d %u",&size, &n);
	data = (long long unsigned int*)  calloc(size, sizeof(long long unsigned int));

	pthread_t *thread_handles;

    thread_count = size;
    thread_handles =  malloc(thread_count * sizeof(pthread_t));
	gettimeofday(&start, NULL);	
    for(thread = 0; thread < thread_count; thread++){
    	pthread_create(&thread_handles[thread], NULL, monte_carlo_pi, (void*) thread);

    }
	
	for(thread = 0; thread < thread_count; thread++)
        pthread_join(thread_handles[thread], NULL);
    
	in = 0;
	for(i = 0; i < thread_count; i++){
		in += data[i];
	}
	gettimeofday(&end, NULL);
	

	
	duracao = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));


		
	pi = 4*in/((double)n);
	printf("%lf\n%lu\n",pi,duracao);
	
	free(thread_handles);

	return 0;
}
