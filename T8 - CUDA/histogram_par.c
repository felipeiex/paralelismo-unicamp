#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>


#define COMMENT "Histogram_GPU"
#define RGB_COMPONENT_COLOR 255

typedef struct {
        unsigned char red, green, blue;
} PPMPixel;

typedef struct {
        int x, y;
        PPMPixel *data;
} PPMImage;

double rtclock()
{
        struct timezone Tzp;
        struct timeval Tp;
        int stat;
        stat = gettimeofday ( &Tp, &Tzp );
        if ( stat != 0 ) printf ( "Error return from gettimeofday: %d",stat );
        return ( Tp.tv_sec + Tp.tv_usec*1.0e-6 );
}


static PPMImage *readPPM ( const char *filename )
{
        char buff[16];
        PPMImage *img;
        FILE *fp;
        int c, rgb_comp_color;
        fp = fopen ( filename, "rb" );
        if ( !fp ) {
                fprintf ( stderr, "Unable to open file '%s'\n", filename );
                exit ( 1 );
        }

        if ( !fgets ( buff, sizeof ( buff ), fp ) ) {
                perror ( filename );
                exit ( 1 );
        }

        if ( buff[0] != 'P' || buff[1] != '6' ) {
                fprintf ( stderr, "Invalid image format (must be 'P6')\n" );
                exit ( 1 );
        }

        img = ( PPMImage * ) malloc ( sizeof ( PPMImage ) );
        if ( !img ) {
                fprintf ( stderr, "Unable to allocate memory\n" );
                exit ( 1 );
        }

        c = getc ( fp );
        while ( c == '#' ) {
                while ( getc ( fp ) != '\n' )
                        ;
                c = getc ( fp );
        }

        ungetc ( c, fp );
        if ( fscanf ( fp, "%d %d", &img->x, &img->y ) != 2 ) {
                fprintf ( stderr, "Invalid image size (error loading '%s')\n", filename );
                exit ( 1 );
        }

        if ( fscanf ( fp, "%d", &rgb_comp_color ) != 1 ) {
                fprintf ( stderr, "Invalid rgb component (error loading '%s')\n",
                          filename );
                exit ( 1 );
        }

        if ( rgb_comp_color != RGB_COMPONENT_COLOR ) {
                fprintf ( stderr, "'%s' does not have 8-bits components\n", filename );
                exit ( 1 );
        }

        while ( fgetc ( fp ) != '\n' )
                ;
        img->data = ( PPMPixel* ) malloc ( img->x * img->y * sizeof ( PPMPixel ) );

        if ( !img ) {
                fprintf ( stderr, "Unable to allocate memory\n" );
                exit ( 1 );
        }

        if ( fread ( img->data, 3 * img->x, img->y, fp ) != img->y ) {
                fprintf ( stderr, "Error loading image '%s'\n", filename );
                exit ( 1 );
        }

        fclose ( fp );
        return img;
}

unsigned char*  Image2Vec ( PPMImage* image )
{
        int n = image->x * image->y;
        unsigned char* vectorized = ( unsigned char* ) malloc ( sizeof ( unsigned char ) * n*3 );
        //Fills the vector with the already normalized data.
        for ( int i = 0; i < n*3; i+=3 ) {
                vectorized[i] = ( image->data[i/3].red *4 ) / 256;
                vectorized[i+1] = ( image->data[i/3].green *4 ) / 256;
                vectorized[i+2] = ( image->data[i/3].blue * 4 ) / 256;
        }
        return vectorized;
}

__global__ void histogram_par ( unsigned char* image, int* hist, int x, int y, int n )
{
        __shared__ int private_hist[64];

        //Initialize the private histogram with zeroes
        if ( ( threadIdx.x * 32 + threadIdx.y ) < 64 )
                private_hist[threadIdx.x * 32 + threadIdx.y] = 0;
        //Wait for all threads
        __syncthreads();

        //Compute the current element index
        int col = blockDim.x * blockIdx.x + threadIdx.x;
        int row = blockDim.y * blockIdx.y + threadIdx.y;
        int index = row *y + col;

        //Computes the histogram position and increments it.
        if ( ( row < x && col < y ) && ( index < n ) ) {
                atomicAdd ( & ( private_hist[image[index*3] * 16 + image[index*3 + 1]*4 + image[index*3 + 2]] ),1 );
        }

        //Wait for all other threads in the block to finish
        __syncthreads();

        //Agregates the local histogram to the global one.
        if ( ( threadIdx.x * 32 + threadIdx.y ) < 64 ) {
                atomicAdd ( & ( hist[threadIdx.x * 32 + threadIdx.y] ), private_hist[threadIdx.x * 32 + threadIdx.y] );
        }
}




int main ( int argc, char *argv[] )
{

        if ( argc != 2 ) {
                printf ( "Too many or no one arguments supplied.\n" );
        }

        //Timers
        cudaEvent_t start, stop;
        cudaEventCreate ( &start );
        cudaEventCreate ( &stop );


        double t_start, t_end;
        int i;
        char *filename = argv[1]; //Recebendo o arquivo!;


        //Vetorizamos a imagem
        PPMImage *image = readPPM ( filename );
        int x = image->x;
        int y = image->y;
        int n = x*y;

        unsigned char* vect_image = Image2Vec ( image );

        // Iniatialize variables
        int *hist_par;
        unsigned char *image_par;

        // Alloc CPU memory
        int *hist = ( int* ) malloc ( sizeof ( int ) *64 );

        // Alloc image and histogram in GPU
        cudaEventRecord ( start );
        cudaMalloc ( ( void** ) &image_par, sizeof ( unsigned char ) *n*3 );
        cudaMalloc ( ( void** ) &hist_par, sizeof ( int ) *64 );
        cudaEventRecord ( stop );
        cudaEventSynchronize ( stop );
        float tempo_GPU_criar_buffer = 0;
        cudaEventElapsedTime ( &tempo_GPU_criar_buffer,start,stop );

        // Initialize histogram with zeroes
        for ( i=0; i < 64; i++ ) hist[i] = 0.0;

        // Copy inputs to device
        // Histogram must be copied because it needs to be initialized with zero.

        cudaEventRecord ( start );
        cudaMemcpy ( hist_par, hist, sizeof ( int ) *64 ,cudaMemcpyHostToDevice );
        cudaMemcpy ( image_par, vect_image, sizeof ( unsigned char ) *n*3 ,cudaMemcpyHostToDevice );
        cudaEventRecord ( stop );
        cudaEventSynchronize ( stop );
        float tempo_GPU_offload_enviar = 0;
        cudaEventElapsedTime ( &tempo_GPU_offload_enviar,start,stop );

        // Initialize dimGrid and dimBlocks
        // create a grid with (32 / columns) number of columns and (32 / lines) number of rows
        // the ceiling function makes sure there are enough to cover all elements
        dim3 dimGrid ( ceil ( ( float ) y / 32 ), ceil ( ( float ) x / 32 ), 1 );

        // create a block with 32 columns and 32 rows
        dim3 dimBlock ( 32, 32, 1 );

        // Launch kernel!

        cudaEventRecord ( start );
        histogram_par<<<dimGrid, dimBlock>>> ( image_par, hist_par,x,y,n );
        cudaEventRecord ( stop );
        cudaEventSynchronize ( stop );
        float tempo_GPU_kernel = 0;
        cudaEventElapsedTime ( &tempo_GPU_kernel,start,stop );

        // Copy result to local array
        cudaEventRecord ( start );
        cudaMemcpy ( hist, hist_par, 64 * sizeof ( int ), cudaMemcpyDeviceToHost );
        cudaEventRecord ( stop );
        cudaEventSynchronize ( stop );
        float tempo_GPU_receber = 0;
        cudaEventElapsedTime ( &tempo_GPU_receber,start,stop );


        //Print and normalize
        for ( i = 0; i < 64; i++ ) {
                printf ( "%0.3f ", hist[i]/ ( float ) n );
        }
        float tempo_total = tempo_GPU_criar_buffer + tempo_GPU_offload_enviar + tempo_GPU_kernel + tempo_GPU_receber;
        printf ( "\n cria_buffer %.2f\n offload_send %.2f\n kernel %.2f\n offload_receive %.2f\n total%.2f      %.2f \n",tempo_GPU_criar_buffer,tempo_GPU_offload_enviar,tempo_GPU_kernel,tempo_GPU_receber,tempo_total,tempo_total*0.001);
	printf("\n");
}
