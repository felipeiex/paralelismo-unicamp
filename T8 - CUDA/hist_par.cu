#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>


#define COMMENT "Histogram_GPU"
#define RGB_COMPONENT_COLOR 255

typedef struct {
        unsigned char red, green, blue;
} PPMPixel;

typedef struct {
        int x, y;
        PPMPixel *data;
} PPMimg;

//Funções
double rtclock();
static PPMimg *readPPM ( const char *filename );
unsigned char*  img2Vec ( PPMimg* img );
__global__ void histogram_par ( unsigned char* img, int* hist, int x, int y, int n );



int main ( int argc, char *argv[] )
{

        if ( argc != 2 ) {
                printf ( "Too many or no one arguments supplied.\n" );
        }
        cudaEvent_t start, stop;
        cudaEventCreate ( &start );
        cudaEventCreate ( &stop );


        double t_start, t_end;
        int i;
        char *filename = argv[1];

        PPMimg *img = readPPM ( filename );
        int x = img->x;
        int y = img->y;
        int n = x*y;

        unsigned char* vect_img = img2Vec ( img );
        int *hist_par;
        unsigned char *img_par;

        int *hist = ( int* ) malloc ( sizeof ( int ) *64 );

        cudaEventRecord ( start );
        cudaMalloc ( ( void** ) &img_par, sizeof ( unsigned char ) *n*3 );
        cudaMalloc ( ( void** ) &hist_par, sizeof ( int ) *64 );
        cudaEventRecord ( stop );
        cudaEventSynchronize ( stop );
        float tempo_GPU_criar_buffer = 0;
        cudaEventElapsedTime ( &tempo_GPU_criar_buffer,start,stop );


        for ( i=0; i < 64; i++ ){
                hist[i] = 0.0;
        }

        cudaEventRecord ( start );
        cudaMemcpy ( hist_par, hist, sizeof ( int ) *64 ,cudaMemcpyHostToDevice );
        cudaMemcpy ( img_par, vect_img, sizeof ( unsigned char ) *n*3 ,cudaMemcpyHostToDevice );
        cudaEventRecord ( stop );
        cudaEventSynchronize ( stop );
        float tempo_GPU_offload_enviar = 0;
        cudaEventElapsedTime ( &tempo_GPU_offload_enviar,start,stop );

        dim3 dimGrid ( ceil ( ( float ) y / 32 ), ceil ( ( float ) x / 32 ), 1 );

        dim3 dblock ( 32, 32, 1 );

        cudaEventRecord ( start );
        histogram_par<<<dimGrid, dblock>>> ( img_par, hist_par,x,y,n );
        cudaEventRecord ( stop );
        cudaEventSynchronize ( stop );
        float tempo_GPU_kernel = 0;
        cudaEventElapsedTime ( &tempo_GPU_kernel,start,stop );

        cudaEventRecord ( start );
        cudaMemcpy ( hist, hist_par, 64 * sizeof ( int ), cudaMemcpyDeviceToHost );
        cudaEventRecord ( stop );
        cudaEventSynchronize ( stop );
        float tempo_GPU_receber = 0;
        cudaEventElapsedTime ( &tempo_GPU_receber,start,stop );


        for ( i = 0; i < 64; i++ ) {
                printf ("%0.3f ", (float)(hist[i]/(float)n) );
        }
	printf("\n");
}


double rtclock()
{
        struct timezone Tzp;
        struct timeval Tp;
        int stat;
        stat = gettimeofday ( &Tp, &Tzp );
        if ( stat != 0 ) printf ( "Error return from gettimeofday: %d",stat );
        return ( Tp.tv_sec + Tp.tv_usec*1.0e-6 );
}

static PPMimg *readPPM ( const char *filename )
{
        char buff[16];
        PPMimg *img;
        FILE *fp;
        int c, rgb_comp_color;
        fp = fopen ( filename, "rb" );
        if ( !fp ) {
                fprintf ( stderr, "Unable to open file '%s'\n", filename );
                exit ( 1 );
        }

        if ( !fgets ( buff, sizeof ( buff ), fp ) ) {
                perror ( filename );
                exit ( 1 );
        }

        if ( buff[0] != 'P' || buff[1] != '6' ) {
                fprintf ( stderr, "Invalid img format (must be 'P6')\n" );
                exit ( 1 );
        }

        img = ( PPMimg * ) malloc ( sizeof ( PPMimg ) );
        if ( !img ) {
                fprintf ( stderr, "Unable to allocate memory\n" );
                exit ( 1 );
        }

        c = getc ( fp );
        while ( c == '#' ) {
                while ( getc ( fp ) != '\n' )
                        ;
                c = getc ( fp );
        }

        ungetc ( c, fp );
        if ( fscanf ( fp, "%d %d", &img->x, &img->y ) != 2 ) {
                fprintf ( stderr, "Invalid img size (error loading '%s')\n", filename );
                exit ( 1 );
        }

        if ( fscanf ( fp, "%d", &rgb_comp_color ) != 1 ) {
                fprintf ( stderr, "Invalid rgb component (error loading '%s')\n",
                          filename );
                exit ( 1 );
        }

        if ( rgb_comp_color != RGB_COMPONENT_COLOR ) {
                fprintf ( stderr, "'%s' does not have 8-bits components\n", filename );
                exit ( 1 );
        }

        while ( fgetc ( fp ) != '\n' )
                ;
        img->data = ( PPMPixel* ) malloc ( img->x * img->y * sizeof ( PPMPixel ) );

        if ( !img ) {
                fprintf ( stderr, "Unable to allocate memory\n" );
                exit ( 1 );
        }

        if ( fread ( img->data, 3 * img->x, img->y, fp ) != img->y ) {
                fprintf ( stderr, "Error loading img '%s'\n", filename );
                exit ( 1 );
        }

        fclose ( fp );
        return img;
}
unsigned char*  img2Vec ( PPMimg* img )
{
        int n = img->x * img->y;
        unsigned char* vectorized = ( unsigned char* ) malloc ( sizeof ( unsigned char ) * n*3 );
        for ( int i = 0; i < n*3; i+=3 ) {
                vectorized[i] = ( img->data[i/3].red *4 ) / 256;
                vectorized[i+1] = ( img->data[i/3].green *4 ) / 256;
                vectorized[i+2] = ( img->data[i/3].blue * 4 ) / 256;
        }
        return vectorized;
}

__global__ void histogram_par ( unsigned char* img, int* hist, int x, int y, int n )
{
        __shared__ int private_hist[64];

        if ( ( threadIdx.x * 32 + threadIdx.y ) < 64 )
                private_hist[threadIdx.x * 32 + threadIdx.y] = 0;
        __syncthreads();

        int col = blockDim.x * blockIdx.x + threadIdx.x;
        int row = blockDim.y * blockIdx.y + threadIdx.y;
        int index = row *y + col;
        
        if ( ( row < x && col < y ) && ( index < n ) ) {
                atomicAdd ( & ( private_hist[img[index*3] * 16 + img[index*3 + 1]*4 + img[index*3 + 2]] ),1 );
        }

        __syncthreads();

        if ( ( threadIdx.x * 32 + threadIdx.y ) < 64 ) {
                atomicAdd ( & ( hist[threadIdx.x * 32 + threadIdx.y] ), private_hist[threadIdx.x * 32 + threadIdx.y] );
        }
}
