#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

/*
	Autor: Reinaldo
	RA: 226006

	Comentários:
	1) Primeira tentativa foi mantendo a estrutura do algoritmo e utilizando dois parallel for. Não passou no primeiro caso do Susy (speedup)
	2) Segunda tentativa foi utilizando uma queue, porém a entrada é mto grande então não foi possível alocar o suficiente.
	3) Terceira tentativa dimunuí a complexidade do algoritmo tirando um dos for e otimizando o primeiro for.

*/
void producer_consumer(int *buffer, int size, int *vec, int n, int nt) {
	int i, j;
	long long unsigned int k = 0, sum = 0;

	for(i=0;i<n;i+=2) {
		if(i % 2 == 0) {	// PRODUTOR
			#pragma omp parallel for num_threads(nt) reduction(+:sum) 
			for(j=0;j<size;j++) {
				 sum += vec[i] + j*vec[i+1];
				 //printf("%d\n", sum);
			}
		}
	}
	printf("%llu\n",sum);
}

int main(int argc, char * argv[]) {
	double start, end;
	float d;
	int i, n, size, nt;
	int *buff;
	int *vec;

	scanf("%d %d %d",&nt,&n,&size);
	
	buff = (int *)malloc(size*sizeof(int));
	vec = (int *)malloc(n*sizeof(int));

	for(i=0;i<n;i++)
		scanf("%d",&vec[i]);
	
	start = omp_get_wtime();
	producer_consumer(buff, size, vec, n, nt);
	end = omp_get_wtime();

	printf("%lf\n",end-start);

	free(buff);
	free(vec);

	return 0;
}

/*
gprof

###
arq1.in
Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  Ts/call  Ts/call  name
100.54      0.01     0.01                             main
  0.00      0.01     0.00        1     0.00     0.00  producer_consumer
###
arq2.in
Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  Ts/call  Ts/call  name
100.54      0.28     0.28                             main
  0.00      0.28     0.00        1     0.00     0.00  producer_consumer

###
Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  Ts/call  Ts/call  name
100.54      2.67     2.67                             main
  0.00      2.67     0.00        1     0.00     0.00  producer_consumer
-------------
lscpu

Arquitetura:           x86_64
Modo(s) operacional da CPU:32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Thread(s) per núcleo  1
Núcleo(s) por soquete:4
Soquete(s):            1
Nó(s) de NUMA:        1
ID de fornecedor:      GenuineIntel
Família da CPU:       6
Modelo:                158
Model name:            Intel(R) Core(TM) i5-7300HQ CPU @ 2.50GHz
Step:                  9
CPU MHz:               1893.457
CPU max MHz:           3500,0000
CPU min MHz:           800,0000
BogoMIPS:              4991.91
Virtualização:       VT-x
cache de L1d:          32K
cache de L1i:          32K
cache de L2:           256K
cache de L3:           6144K
NUMA node0 CPU(s):     0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb invpcid_single intel_pt retpoline kaiser tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp

arq1.in
Serial com -O0
12784254000
0.008763

Serial com -O1
12784254000
0.008763

Serial com -O2
12784254000
0.002229

Serial com -O3
12784254000
0.002135


arq2.in
Serial com -O0
12426110415000
0.267274

Serial com -O1
12426110415000
0.077022


Serial com -O2
12426110415000
0.076919

Serial com -O3
12426110415000
0.074627

arq3.in

Serial com -O0
126047050705000
2.471112

Serial com -O1
126047050705000
0.511373

Serial com -O2
126047050705000
0.524034

Serial com -O3
126047050705000
0.530039
*/