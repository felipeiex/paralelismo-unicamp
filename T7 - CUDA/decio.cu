1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
87
88
89
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
 
#define block_size 32
 
__global__ void sumMatrixes(int* A, int* B, int* C, int H, int W){
  int tidx = threadIdx.x + blockDim.x * blockIdx.x;
  int tidy = threadIdx.y + blockDim.y * blockIdx.y;
 
  int pos = tidx * W + tidy;
 
  if(tidy < W && tidx < H){
    C[pos] = A[pos] + B[pos];
  }
}
 
int main()
{
    int *A, *B, *C;
    int i, j;
 
    //Input
    int linhas, colunas;
 
    scanf("%d", &linhas);
    scanf("%d", &colunas);
 
    //Alocando memória na CPU
    A = (int *)malloc(sizeof(int)*linhas*colunas);
    B = (int *)malloc(sizeof(int)*linhas*colunas);
    C = (int *)malloc(sizeof(int)*linhas*colunas);
     
    //Inicializar
    for(i = 0; i < linhas; i++){
        for(j = 0; j < colunas; j++){
            A[i*colunas+j] =  B[i*colunas+j] = i+j;
        }
    }
 
//tentamos alocar espaco de memoria para as matrizes na GPU, se der erro, mostramos o erro na saida padrao.
    int* _A, *_B, *_C;
    int matrix_size = sizeof(int) * linhas * colunas;
 
    cudaMalloc((void**)&_A, matrix_size);
    cudaMalloc((void**)&_B, matrix_size);
    cudaMalloc((void**)&_C, matrix_size);
 
    cudaMemcpy(_A, A, matrix_size, cudaMemcpyHostToDevice);
    cudaMemcpy(_B, B, matrix_size, cudaMemcpyHostToDevice);
 
 
    dim3 dimGrid(1, 1);
    dim3 dimBlock(block_size, block_size);
 
    dimGrid.x = ((linhas / block_size) + ( (linhas % block_size == 0 ? 0 : 1) ) );
    dimGrid.y = ((colunas / block_size) + ( (colunas % block_size == 0 ? 0 : 1) ) );
 
    sumMatrixes <<< dimGrid, dimBlock >>> (_A, _B, _C, linhas, colunas); 
 
    cudaMemcpy(C, _C, matrix_size, cudaMemcpyDeviceToHost);
 
 
    //Computacao que deverá ser movida para a GPU (que no momento é executada na CPU)
    //Lembrar que é necessário usar mapeamento 2D (visto em aula) 
    // for(i=0; i < linhas; i++){
    //     for(j = 0; j < colunas; j++){
    //         C[i*colunas+j] = A[i*colunas+j] + B[i*colunas+j];
    //     }
    // }
 
    long long int somador=0;
    //Manter esta computação na CPU
    for(i = 0; i < linhas; i++){
        for(j = 0; j < colunas; j++){
            somador+=C[i*colunas+j];   
        }
    }
     
    printf("%lli\n", somador);
 
    free(A);
    free(B);
    free(C);
 
    cudaFree(_A);
    cudaFree(_B);
    cudaFree(_C);
}