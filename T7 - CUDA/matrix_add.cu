#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//kernel para o cuda
__global__ void add(int linhas, int colunas, int *A, int *B, int *C)
{
    int i = 0;
    int j = 0;
    //int index = threadIdx.x;
    //int stride = blockDim.x;

    //divide o calculo da soma de cada linha da matriz C entre "linhas" threads
    //for(i=index*(linhas/stride); i < index*(linhas/stride)+(linhas/stride); i++)
    for(i=0; i < linhas; i++)
    {
        for(j = 0; j < colunas; j++)
        {
            C[i*colunas+j] = A[i*colunas+j] + B[i*colunas+j];
        }
    }
}


int main()
{
    int *A, *B, *C;
    int *dA, *dB, *dC;
    int i, j;

    //Input
    int linhas, colunas;

    scanf("%d", &linhas);
    scanf("%d", &colunas);

    //Alocando memória na CPU
    A = (int *)malloc(sizeof(int)*linhas*colunas);
    B = (int *)malloc(sizeof(int)*linhas*colunas);
    C = (int *)malloc(sizeof(int)*linhas*colunas);

    //Alocando memória na GPU
    cudaMalloc((void **)&dA, sizeof(int)*linhas*colunas);
    cudaMalloc((void **)&dB, sizeof(int)*linhas*colunas);
    cudaMalloc((void **)&dC, sizeof(int)*linhas*colunas);
    
    //Inicializar
    for(i = 0; i < linhas; i++)
    {
        for(j = 0; j < colunas; j++)
        {
            A[i*colunas+j] =  B[i*colunas+j] = i+j;
        }
    }

    //make a copy of A, B, C to the device memory
    cudaMemcpy(dA, A, (linhas*colunas)*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dB, B, (linhas*colunas)*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dC, C, (linhas*colunas)*sizeof(int), cudaMemcpyHostToDevice);

    //add <<<1, linhas>>>(linhas,colunas,dA,dB,dC);
    add <<<1, 1>>>(linhas,colunas,dA,dB,dC);

    //make a copy of A, B, C to the main memory
    cudaMemcpy(C, dC, (linhas*colunas)*sizeof(int), cudaMemcpyDeviceToHost);
    
    long long int somador=0;
    //Manter esta computação na CPU
    for(i = 0; i < linhas; i++)
    {
        for(j = 0; j < colunas; j++)
        {
            somador+=C[i*colunas+j];   
        }
    }
    
    printf("%lli\n", somador);

    // Free memory
    cudaFree(A);
    cudaFree(B);
    cudaFree(C);
}