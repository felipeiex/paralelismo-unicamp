#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

void producer_consumer(int *buffer, int size, int *vec, int n, int nt) {
    int i;
    
    for(i = 0; i < 10; i++){
        Send_msg();
        Try_receive();
    }


int main(int argc, char * argv[]) {
	double start, end;
	float d;
	int i, n, size, nt;
	int *buff;
	int *vec;

	scanf("%d %d %d",&nt,&n,&size);
	
	buff = (int *)malloc(size*sizeof(int));
	vec = (int *)malloc(n*sizeof(int));

	for(i=0;i<n;i++)
		scanf("%d",&vec[i]);
	
	start = omp_get_wtime();
	producer_consumer(buff, size, vec, n, nt);
	end = omp_get_wtime();

	printf("%lf\n",end-start);

	free(buff);
	free(vec);

	return 0;
}
