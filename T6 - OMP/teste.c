#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <omp.h>
/*
gcc senha-para.c -o para -fopenmp

Serial

22.212495
213.549549
696.625662cat 


omp for
14.89
174.15

omp task
15.23
150.59

*/
FILE *popen(const char *command, const char *type);

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

int main ()
{
   FILE * fp;
   char finalcmd[300] = "unzip -P%d -t %s 2>&1";
   int nt;

   char filename[100];
   char ret[200];
   char cmd[400];
   double t_start, t_end;
   unsigned seed;
   int i;
   scanf("%d", &nt);
   scanf("%s", filename);

   t_start = rtclock();
   int termino = 0;
   int randi[nt];
   int rand = 0;
   randi[0] = 0;
   randi[nt-1] = 500000/2;
   for(i = 1; i < nt-1; i++)
        randi[i] = rand_r(&seed) % 500000;

   #pragma omp parallel num_threads(nt) private(i,rand) shared(termino)
   while(!termino){    
    if(!rand){         
        i = randi[omp_get_thread_num()];
        rand = 1;
    }
    printf("\nThread: %d", omp_get_thread_num());
    printf(" Ï: %d\n", i);
    if(i == 250005) termino = 1;
    i++;
  }
  t_end = rtclock();
 
  fprintf(stdout, "%0.6lf\n", t_end - t_start);  
}
