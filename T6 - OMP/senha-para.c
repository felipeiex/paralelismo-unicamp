#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>
#include <omp.h>
/*
gcc senha-para.c -o para -fopenmp

Serial

22.212495
213.549549
696.625662cat 


omp for
14.89
174.15

omp task
15.23
150.59

*/
FILE *popen(const char *command, const char *type);

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

int main ()
{
   FILE * fp;
   char finalcmd[300] = "unzip -P%d -t %s 2>&1";
   int nt;

   char filename[100];
   char ret[200];
   char cmd[400];
   double t_start, t_end;

   int i = 0;
   scanf("%d", &nt);
   scanf("%s", filename);

   t_start = rtclock();
   #pragma omp parallel num_threads(nt)
   {
    while(i < 500000){
        #pragma omp single
        {
            i++;
            //printf("\ni: %d\n",i );
            sprintf((char*)&cmd, finalcmd, i, filename);
            fp = popen(cmd, "r");	
            while (!feof(fp)) {
                fgets((char*)&ret, 200, fp);
                if (strcasestr(ret, "ok") != NULL) {
                        printf("Senha:%d\n", i);
                        i = 500000;
                
                }
            }
            pclose(fp);
        }
    }
  }
  t_end = rtclock();

  fprintf(stdout, "%0.6lf\n", t_end - t_start);  
}
