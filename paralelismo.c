#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

    

/*
#	pragma	omp	parallel	
! 
! 
Diretiva	paralela	mais	básica.	
O	número	de	threads	que	executam	o	bloco	
que	segue	o	pragma	é	determinado	pelo	
sistema	de	runtime.	


entrada:
4

saída:
Hello from thread 2 of 4
Hello from thread 3 of 4
Hello from thread 0 of 4
Hello from thread 1 of 4

*/

void Hello(void);


int main(int argc, char *argv[]){
    int thread_count = strtol(argv[1], NULL, 10);

    #pragma omp parallel num_threads(thread_count)
    Hello();

    return 0;

}

void Hello(void){
    int my_rank =  omp_get_thread_num();
    int thread_count = omp_get_num_threads();

    printf("Hello from thread %d of %d\n, my_rank, thread_count);
}
