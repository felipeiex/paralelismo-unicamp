#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define fore(i,n) for(int i = 0; i < n; i++)
    

/*
#	pragma	omp	parallel	
! 
! 
Diretiva	paralela	mais	básica.	
O	número	de	threads	que	executam	o	bloco	
que	segue	o	pragma	é	determinado	pelo	
sistema	de	runtime.	


entrada:
4

saída:
Hello from thread 2 of 4
Hello from thread 3 of 4
Hello from thread 0 of 4
Hello from thread 1 of 4

*/

void count_sort_paral(double *a, int n, double *temp);

int main(){
    int nThreads, nElements;

    // Ínicio da Entra
    scanf("%d %d", &nThreads, &nElements);

    double vetor[nElements+1];
    vetor[nElements] = ' ';

    fore(i, nElements){
        scanf("%lf", &vetor[i]);
    }

    //Fim da Entrada

    //Ordenação
    /*
        O vetor temp vai ser utilizado por cada thread.
        Pelo fato de cada número ter sua  posição na memoria, não é necessário utilizar o OMP Warning para lidar com região crítica.
    */
    double *temp;
    temp = (double *)malloc(nElements*sizeof(double));
    double start, end, duracao;
    start = omp_get_wtime();
    //Torna a funcao count_sort_paral paralela
    #pragma omp parallel num_threads(nThreads)
    count_sort_paral(vetor, nElements, temp);

	end = omp_get_wtime();
    duracao = end - start;
    //Copia o resultado para o vetor principal
    memcpy(vetor, temp, nElements*sizeof(double));
	free(temp);

    //Saída
    fore(i, nElements){
        printf("%.2lf ", vetor[i]);
    }
    printf("\n");
    printf("%lf\n", duracao);



    return 0;

}

void count_sort_paral(double *a, int n, double *temp){
    /*
        Algumas alterações foram feitas para tornar o algoritmo de ordenação paralelo, tais como:
         1) tornar o vetor temp externo para que possa ser compartilhado
         2) Dividir o problema em fatia iguais de n threads
         3) Cada fatia ir de i = thread até n/thread_count que é o tamanho de cada fatia.
        O restante do algorítmo permaneceu igual.
    */
    int my_rank =  omp_get_thread_num();
    int thread_count = omp_get_num_threads();
	int i, j, count, i_aux;


	for (i_aux = 0; i_aux < (int)n/thread_count; i_aux++) {
        i = i_aux + my_rank*(n/thread_count);
       // printf("i_aux: %d, i:%d thread_cout: %d, myrank: %d\n", i_aux, i, thread_count, my_rank);
		count = 0;
		for (j = 0; j < n; j++)
			if (a[j] < a[i])
				count++;
			else if (a[j] == a[i] && j < i)
				count++;
		temp[count] = a[i];
	}
}