#include <stdio.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <pthread.h>

/* funcao que calcula o minimo valor em um vetor */
int thread_count;
long thread;
pthread_t *thread_handles;

double h, *val, max, min;
int n, nval, i, *vet, size;
long unsigned int duracao;
struct timeval start, end;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

double min_val(double * vet,int nval) {
	int i;
	double min;

	min = FLT_MAX;


	for(i=0;i<nval;i++) {
		if(vet[i] < min)
			min =  vet[i];
	}
	
	return min;
}

/* funcao que calcula o maximo valor em um vetor */
double max_val(double * vet, int nval) {
	int i;
	double max;

	max = FLT_MIN;

	for(i=0;i<nval;i++) {
		if(vet[i] > max)
			max =  vet[i];
	}
	
	return max;
}

/* conta quantos valores no vetor estao entre o minimo e o maximo passados como parametros */
void * count(void *rank) {
	int i, j, count;
	long my_rank = (long) rank;
	double min_t, max_t;
	int nbins = n;
	int j_aux;
	//printf("Hello2 from thread %ld of %d\n", my_rank, thread_count);
	//for(j = 0; j < nbins; j++){
	/*
		 1) Dividi o problema em fatia iguais de n threads
         2) Cada fatia ir de i = thread até n/thread_count que é o tamanho de cada fatia.
	*/
	int new_size = (int) (nbins/thread_count);
	if(my_rank == 0) new_size +=  nbins%thread_count;

	for(j_aux=0;j_aux < new_size;j_aux++) {
		j = j_aux + my_rank*(nbins/thread_count);
		count = 0;
		min_t = min + j*h;
		max_t = min + (j+1)*h;
		for(i = 0; i < nval ;i++) {
		//for(i_aux = 0; i_aux < (int) (nval/thread_count);i_aux++){
			//i = i_aux + my_rank*(nval/thread_count);
			if( (val[i] <= max_t && val[i] > min_t) || (j == 0 && val[i] <= min_t) ) {
				count++;
			}
		}
		pthread_mutex_lock(&mutex);
		vet[j] = count;/**/
		pthread_mutex_unlock(&mutex);
	}

	
}

int main(int argc, char * argv[]) {
	scanf("%d",&size);

	/* entrada do numero de dados */
	scanf("%d",&nval);
	/* numero de barras do histograma a serem calculadas */
	scanf("%d",&n);

	/* vetor com os dados */
	val = (double *)malloc(nval*sizeof(double));
	vet = (int *)malloc(n*sizeof(int));

	/* entrada dos dados */
	for(i=0;i<nval;i++) {
		scanf("%lf",&val[i]);
	}

	/* calcula o minimo e o maximo valores inteiros */
	min = floor(min_val(val,nval));
	max = ceil(max_val(val,nval));

	/* calcula o tamanho de cada barra */
	h = (max - min)/n;

	gettimeofday(&start, NULL);

	/* chama a funcao */

    pthread_t *thread_handles;

    thread_count = size;
    thread_handles =  malloc(thread_count * sizeof(pthread_t));

    for(thread = 0; thread < thread_count; thread++){
    	pthread_create(&thread_handles[thread], NULL, count, (void*) thread);

    }
	
    for(thread = 0; thread < thread_count; thread++)
        pthread_join(thread_handles[thread], NULL);

    free(thread_handles);
/*
	count((void *) 1);*/

	gettimeofday(&end, NULL);

	duracao = ((end.tv_sec * 1000000 + end.tv_usec) - \
	(start.tv_sec * 1000000 + start.tv_usec));

	printf("%.2lf",min);	
	for(i=1;i<=n;i++) {
		printf(" %.2lf",min + h*i);
	}
	printf("\n");

	/* imprime o histograma calculado */	
	printf("%d",vet[0]);
	for(i=1;i<n;i++) {
		printf(" %d",vet[i]);
	}
	printf("\n");
	
	
	/* imprime o tempo de duracao do calculo */
	printf("%lu\n",duracao);

	free(vet);
	free(val);

	return 0;
}
/*

Tn = Tserial / Tparallel
E = Tn / p
P = 4
E = Tn / p

c
=======================================================
        |Threads   |    2   |    4   |    8   |   16   |
-------------------------------------------------------|
arq1.in |Speedup   | 1,5983 | 0,8244 | 0.7904 | 0.7841 |
		|Eficiencia| 0,3995 | 0,2061 | 0,1976 | 0,1960 |
------------------------------------------------------ |
arq2.in |Speedup   | 1.3508 | 1.7778 | 1,8724 | 1.8028 |
		|Eficiencia| 0,3377 | 0,4444 | 0,4681 | 0,4507 |
------------------------------------------------------ |
arq3.in |Speedup   | 1.8324 | 2,4783 | 3.1777 | 3,3552 |
		|Eficiencia| 0,4581 | 0,6195 | 0,7944 | 0,8388 |
=======================================================

-------------------------------------------------------------------
arq1.in
#T4#
 Performance counter stats for 'gzip bin':

          3,516420      task-clock (msec)         #    0,002 CPUs utilized
                 2      context-switches          #    0,569 K/sec
                 0      cpu-migrations            #    0,000 K/sec
                89      page-faults               #    0,025 M/sec
         2.790.996      cycles                    #    0,794 GHz
   <not supported>      stalled-cycles-frontend
   <not supported>      stalled-cycles-backend
         2.891.381      instructions              #    1,04  insns per cycle
           609.792      branches                  #  173,413 M/sec
            31.182      branch-misses             #    5,11% of all branches
   <not supported>      L1-dcache-loads
   <not supported>      L1-dcache-load-misses
   <not supported>      LLC-loads
   <not supported>      LLC-load-misses

       1,580166163 seconds time elapsed

#T8#
 Performance counter stats for 'gzip bin':

          3,596653      task-clock (msec)         #    0,002 CPUs utilized
                 3      context-switches          #    0,834 K/sec
                 1      cpu-migrations            #    0,278 K/sec
                90      page-faults               #    0,025 M/sec
         2.851.156      cycles                    #    0,793 GHz
   <not supported>      stalled-cycles-frontend
   <not supported>      stalled-cycles-backend
         2.900.573      instructions              #    1,02  insns per cycle
           612.609      branches                  #  170,328 M/sec
            32.121      branch-misses             #    5,24% of all branches
   <not supported>      L1-dcache-loads
   <not supported>      L1-dcache-load-misses
   <not supported>      LLC-loads
   <not supported>      LLC-load-misses

       2,246408141 seconds time elapsed

#T16#
 Performance counter stats for 'gzip bin':

          3,471783      task-clock (msec)         #    0,003 CPUs utilized
                 1      context-switches          #    0,288 K/sec
                 0      cpu-migrations            #    0,000 K/sec
                92      page-faults               #    0,026 M/sec
         2.759.206      cycles                    #    0,795 GHz
   <not supported>      stalled-cycles-frontend
   <not supported>      stalled-cycles-backend
         2.901.552      instructions              #    1,05  insns per cycle
           612.535      branches                  #  176,432 M/sec
            31.122      branch-misses             #    5,08% of all branches
   <not supported>      L1-dcache-loads
   <not supported>      L1-dcache-load-misses
   <not supported>      LLC-loads
   <not supported>      LLC-load-misses

       1,131583621 seconds time elapsed

---------------------------
Com entradas menores, o custo para se criar threads é maior que o custo de realizar as operações necessárias para resolver serialmente, logo não compensa nesses exemplos.

*/